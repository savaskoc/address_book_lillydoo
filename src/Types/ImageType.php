<?php

namespace App\Types;

use App\Services\ImageService;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class ImageType extends StringType
{
    /**
     * @var ImageService
     */
    protected $imageService;

    public function setImageService(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $value ? $this->imageService->fromString($value) : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value ? $value->getPath() : null;
    }
}
