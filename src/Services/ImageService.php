<?php

namespace App\Services;

use App\Image\FilesystemWrapper;
use App\Image\Image;
use Knp\Bundle\GaufretteBundle\FilesystemMap;
use Symfony\Component\HttpFoundation\File\File;

class ImageService
{
    const DEFAULT_DELIMITER = '/';

    protected $filesystems;

    public function __construct(FilesystemMap $filesystems)
    {
        $this->filesystems = $filesystems;
    }

    public function fromString(string $key, string $delimiter = self::DEFAULT_DELIMITER): Image
    {
        [$filesystem, $path] = explode($delimiter, $key, 2);
        return new Image($this->getFilesystem($filesystem), $path);
    }

    public function fromFile(File $file, string $filesystem): ?Image
    {
        $path = sprintf('%s.%s', uniqid(), $file->guessExtension());
        if (copy($file->getRealPath(), "gaufrette://$filesystem/$path")) {
            return new Image($this->getFilesystem($filesystem), $path);
        }
        return null;
    }

    protected function getFilesystem(string $filesystem): FilesystemWrapper
    {
        return new FilesystemWrapper($this->filesystems->get($filesystem), $filesystem);
    }
}
