<?php

namespace App\Image;

use App\Services\ImageService;
use Gaufrette\FilesystemInterface;

/**
 * @see FilesystemInterface
 */
class FilesystemWrapper
{
    protected $delimiter;
    protected $name;

    protected $wrapped;

    public function __construct(FilesystemInterface $wrapped, string $name, string $delimiter = ImageService::DEFAULT_DELIMITER)
    {
        $this->delimiter = $delimiter;
        $this->name = $name;

        $this->wrapped = $wrapped;
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->wrapped, $name], $arguments);
    }

    public function getPath(string $path)
    {
        return implode($this->delimiter, [$this->name, $path]);
    }
}
