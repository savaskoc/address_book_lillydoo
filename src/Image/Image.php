<?php

namespace App\Image;

class Image
{
    protected $filesystem;
    protected $path;

    public function __construct(FilesystemWrapper $filesystem, string $path)
    {
        $this->filesystem = $filesystem;
        $this->path = $path;
    }

    public function getRealPath(): string
    {
        return $this->filesystem->resolve($this->path);
    }

    public function getPath(): string
    {
        return $this->filesystem->getPath($this->path);
    }

    public function remove(): bool
    {
        return $this->filesystem->delete($this->path);
    }
}
